import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Reviews } from '../models/reviews.model';

@Injectable({
  providedIn: 'root'
})
export class ReviewsService {

  constructor(private http: HttpClient) { }

  private reviewsEndpoint: string = 'http://localhost:8081/api/reviews';
  
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': "*"
    })
  };

  getReviews(): Observable<Reviews[]> {
    return this.http.get(this.reviewsEndpoint, this.httpOptions)
      .pipe(map(res => <Reviews[]>res));
  }

  postNewReview(reviews: Reviews): Observable<Reviews> {
    return this.http.post<Reviews>(this.reviewsEndpoint,
      {
        date: reviews.date,
        reviewer: reviews.reviewer,
        comment: reviews.comment
      },
      this.httpOptions);
  }

}
