import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Services } from '../models/services.model';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  
  constructor(private http: HttpClient) { }

  private servicesEndpoint: string = 'http://localhost:8081/api/services';
  
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': "*"
    })
  };

  getServices(): Observable<Services[]> {
    return this.http.get(this.servicesEndpoint, this.httpOptions)
      .pipe(map(res => <Services[]>res));
  }

  getServicesById(CatId: string): Observable<Services[]> {
    console.log(CatId);
    return this.http.get(this.servicesEndpoint + "/bycategory/" + CatId, this.httpOptions)
      .pipe(map(res => <Services[]>res));

  }
}


