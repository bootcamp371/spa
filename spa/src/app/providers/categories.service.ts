import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Categories } from '../models/categories.model';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http: HttpClient) { }

  private categoryEndpoint: string = 'http://localhost:8081/api/categories';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': "*"
    })
  };

  getCategories() : Observable<Categories[]> {
    return this.http.get(this.categoryEndpoint, this.httpOptions)
    .pipe(map(res => <Categories[]>res));
  }

}
