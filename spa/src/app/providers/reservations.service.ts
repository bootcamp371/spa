import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Reservations } from '../models/reservations.model';

@Injectable({
  providedIn: 'root'
})
export class ReservationsService {

  constructor(private http: HttpClient) { }

  private reservationsEndpoint: string = 'http://localhost:8081/api/reservations';
  
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': "*"
    })
  };

  postNewReservation(reservations: Reservations): Observable<Reservations> {
    return this.http.post<Reservations>(this.reservationsEndpoint,
      {
        date: reservations.date,
        name: reservations.name,
        time: reservations.time,
        phone: reservations.phone
      },
      this.httpOptions);
  }
}
