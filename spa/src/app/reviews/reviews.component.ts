import { Component, OnInit } from '@angular/core';
import { Reviews } from '../models/reviews.model';
import { ReviewsService } from '../providers/reviews.service';
import { Router } from '@angular/router';
//import { __makeTemplateObject } from 'tslib';

declare var window: any;

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent {
  wReviews: Array<Reviews> = [];
  formModal: any;
  userName: string = "";
  userComments: string = "";
  theDate = new Date();
  date: string = "";

  constructor(private router: Router, private getReviews: ReviewsService) { }

  ngOnInit(): void {

    this.getReviews.getReviews().subscribe(data => {
      this.wReviews = data;
    });
  }

  addReview() {
    
    console.log(this.userName);
    console.log(this.userComments);
  
    this.date = this.theDate.toLocaleDateString();
    this.getReviews.postNewReview(new Reviews(" ", this.userName, this.userComments)).subscribe(data => console.dir(data));

    
    this.formModal.hide();
  }

  openModal() {
    this.formModal = new window.bootstrap.Modal(
      document.getElementById("addReview")
    );
    this.formModal.show()
  }

}
