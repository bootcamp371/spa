import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Services } from '../models/services.model';
import { ServicesService } from '../providers/services.service';
import { Reservations } from '../models/reservations.model';
import { ReservationsService } from '../providers/reservations.service';


declare var window: any;

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {

  wServices: Array<Services> = [];
  CatId: string = "";
  formModal: any;
  catName: string = "";
  isDivVisible: boolean = false;
  userName: string = "";
  phoneNumber: string = "";
  resDate = new Date();
  resTime: string = "";
  buttonText: string = "";
  dateString: string = "";

  constructor(
    private activatedRoute: ActivatedRoute,
    private servicesService: ServicesService,
    private reservationsService: ReservationsService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.CatId = params['catId'];
      this.catName = params['catName'];
    });


    this.servicesService.getServicesById(this.CatId).subscribe(
      data => {
        this.wServices = data
      }

    );
  }
  
  openModal(serviceId: string) {
    this.formModal = new window.bootstrap.Modal(
      document.getElementById(serviceId)
    );
    this.isDivVisible = false;
    this.formModal.show()
  }

  makeReservation() {
    this.isDivVisible = true;
  }

  submitReservation() {
    this.dateString = this.resDate.toLocaleString();
    this.reservationsService.postNewReservation(new Reservations(this.dateString, this.userName, this.resTime, this.phoneNumber)).subscribe(data => console.dir(data));
    this.formModal.hide();
  }

}

