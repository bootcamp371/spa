import { Component, OnInit } from '@angular/core';

import { Reviews } from '../models/reviews.model';
import { ReviewsService } from '../providers/reviews.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{
  wReviews: Array<Reviews> = [];

  constructor(private getReviews: ReviewsService) { }

  ngOnInit(): void {

    this.getReviews.getReviews().subscribe(data => {
      this.wReviews = data;
    });
  }


}
