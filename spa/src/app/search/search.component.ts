import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Categories } from '../models/categories.model';
import { CategoriesService } from '../providers/categories.service';



@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  wCategories: Array<Categories> = [];
  

  constructor(private router: Router, private getCategories: CategoriesService) { }

  ngOnInit(): void {
    
    this.getCategories.getCategories().subscribe(data => {
      this.wCategories = data;

    });
  }
  seeServices(catId: string, catName: string): void {
    // console.log(catId);
    this.router.navigate(['/services'],
      {
        queryParams: {
          catId: catId,
          catName: catName
        }
      }
    );
  }

}
